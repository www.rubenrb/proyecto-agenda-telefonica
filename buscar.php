<!DOCTYPE html>
<html>
<head>

<style>

#rojo{
  color:#EE280C;
}

.myButton {
  box-shadow: 0px 1px 0px 0px #9acc85;
  background:linear-gradient(to bottom, #74ad5a 5%, #68a54b 100%);
  background-color:#74ad5a;
  border-radius:6px;
  border:1px solid #3b6e22;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  
}
.myButton:hover {
  background:linear-gradient(to bottom, #68a54b 5%, #74ad5a 100%);
  background-color:#68a54b;
}
.myButton:active {
  position:relative;
  top:1px;
}

.myButton1 {
  box-shadow: 0px 1px 0px 0px #fff6af;
  background:linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
  background-color:#ffec64;
  border-radius:6px;
  border:1px solid #ffaa22;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffee66;
}
.myButton1:hover {
  background:linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
  background-color:#ffab23;
}
.myButton1:active {
  position:relative;
  top:1px;
}

.campo{
   -moz-box-shadow: inset -3px -3px 10px #eee;
   -webkit-box-shadow: inset -3px -3px 10px #eee;
   box-shadow: inset -3px -3px 10px #eee;
   border-radius: 15px;
   height: 20px;
}
.campo::-webkit-input-placeholder {
  padding-left: 5px;
}

#texto {
  font-family: "Trebuchet MS", Verdana, sans-serif;
  margin-left:5%;
}

#texto1{
  display: inline-block;
  vertical-align: top;
}

.cabecera{
  color:white;
  background-color:#6C969B;
  padding-left:15px;
  padding-bottom:15px;
}

.myButton3 {
  box-shadow:inset 0px 1px 0px 0px #ffffff;
  background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
  background-color:#ffffff;
  border-radius:6px;
  border:1px solid #dcdcdc;
  display:inline-block;
  cursor:pointer;
  color:#666666;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffffff;
}
.myButton3:hover {
  background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
  background-color:#f6f6f6;
}
.myButton3:active {
  position:relative;
  top:1px;
}

table{
  
  border-collapse: collapse;
  background-color:#F5C274;
  border-radius: 15px;
  margin: auto;
  width:70%
}

</head>
<body>

<?php
  session_start();
  if(isset($_SESSION["usuario"])){


?>


<div class="cabecera">


  <img src="imagenes/img1.png" width="90" height="90"><h1 id="texto1">&nbsp;Agenda de contactos </h1>
  <br><br><br>


  <form  action="" method="post">

   <input class="myButton3" type="submit" value="Volver" name="volver">
   <input class="myButton3" type="submit" value="Añadir registro"  name="anadir">
   <input class="myButton3" type="submit" value="Listar"  name="listar">
   <input class="myButton3" type="submit" value="Borrar"  name="borrar">
   <input class="myButton3" type="submit" value="Buscar"  name="buscar">
   <input class="myButton3" type="submit" value="Modificar"  name="modificar">
   <input class="myButton3" type="submit" value="Borrar todo"  name="borrartodo">
  </form>

</div>

<?php


    if (!isset($_POST['busqueda'])){

?>
      <div id="texto">


      <br><br>
      <img src="imagenes/img5.png" width="30" height="30"><p id="texto1"><b>&nbsp;&nbsp;Rellena cualquier campo para buscar un contacto</b></p><br>
      <form action="<?php ($_SERVER["PHP_SELF"]);?>" method="post">

        <p><b>Nombre</b></p> <input class="campo" name="usuario" type="text" placeholder="Ruben" align="center">
      
      <p><b>Apellidos</b></p> <input class="campo" name="apellido" type="text" placeholder="Rodriguez">
      
      <p><b>Teléfono</b></p> <input class="campo" name="tlf" type="text" placeholder="123456789">

      <p><b>Correo</b></p> <input class="campo" name="correo" type="text" placeholder="ejemplo@gmail.com">
      <br><br>
        

      
      
      <input class="myButton" type="submit" value="Buscar"  name="busqueda">
      <input  class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">

      </form>


<?php
    }
  }

	$servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "agenda";


	$conn = mysqli_connect($servername, $username, $password,$dbname);

	if (!$conn) {
	 die("Connection failed: " . mysqli_connect_error());
	}

if (isset($_POST['busqueda'])){
	
		$usuario=$_POST["usuario"];
		$apellido=$_POST["apellido"];
		$tlf=$_POST["tlf"];
		$correo=$_POST["correo"];

		$sql = "SELECT * FROM usuarios WHERE nombre='$usuario' OR apellidos='$apellido' OR telefono='$tlf' OR correo='$correo'";

		$result = mysqli_query ($conn, $sql);

           if(mysqli_num_rows($result) > 0){

          
              echo "<h3 align='center'><b>Lista de contactos encontrados con los criterios de busqueda</b></h3><br>";
              echo "<table style='width:70%'>";
              echo "<tr>";
              echo "<th>" ."Nombre: "."</th>";
              echo "<th>" ."Apellidos: "."</th>";
              echo "<th>" ." Telefono: "."</th>";
              echo "<th>" ." Correo: "."</th>";
              echo "</tr>";


             while ($registro = mysqli_fetch_row($result)) {
              echo "<tr align='center'>";
              echo "<td>" .$registro[0]."</td>";
              echo "<td>" .$registro[1]."</td>";
              echo "<td>" .$registro[2]."</td>";
              echo "<td>" .$registro[3]."</td>";
              echo "</tr>";


            }
            echo "</table>"."<br>";
          }else{
  
           echo "<br>No se han encontrado registros";
          
        }

	
}
	if (isset($_POST['volver'])){


  header("location:menu1.php");
  }

   if (isset($_POST['anadir'])){


  header("location:anadir.php");
  }

 if (isset($_POST['listar'])){


  header("location:listar.php");
  }

 if (isset($_POST['borrar'])){


  header("location:borrar.php");
  }

 if (isset($_POST['buscar'])){


  header("location:buscar.php");
  }

 if (isset($_POST['modificar'])){


  header("location:modificar.php");
  }

 if (isset($_POST['borrartodo'])){
   
  header("location:borrartodo.php");
  }
mysqli_close($conn);
?>

</div>
</body>
</html>
