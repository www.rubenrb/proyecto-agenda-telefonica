<!DOCTYPE html>
<html lang="en">

<head>

    <style>

    #texto1{
  display: inline-block;
  vertical-align: top;
}

.cabecera{
  color:white;
  background-color:#6C969B;
  padding-left:15px;
  padding-bottom:15px;
}

.myButton3 {
  box-shadow:inset 0px 1px 0px 0px #ffffff;
  background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
  background-color:#ffffff;
  border-radius:6px;
  border:1px solid #dcdcdc;
  display:inline-block;
  cursor:pointer;
  color:#666666;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffffff;
}
.myButton3:hover {
  background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
  background-color:#f6f6f6;
}
.myButton3:active {
  position:relative;
  top:1px;
}

.myButton1 {
  box-shadow: 0px 1px 0px 0px #fff6af;
  background:linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
  background-color:#ffec64;
  border-radius:6px;
  border:1px solid #ffaa22;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffee66;
}
.myButton1:hover {
  background:linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
  background-color:#ffab23;
}
.myButton1:active {
  position:relative;
  top:1px;
}

.campo{
   -moz-box-shadow: inset -3px -3px 10px #eee;
   -webkit-box-shadow: inset -3px -3px 10px #eee;
   box-shadow: inset -3px -3px 10px #eee;
   border-radius: 15px;
   height: 20px;
}


table{
  
  border-collapse: collapse;
  background-color:#F5C274;
  border-radius: 15px;
  margin: auto;
  width:70%
}

.myButton {
  box-shadow: 0px 1px 0px 0px #9acc85;
  background:linear-gradient(to bottom, #74ad5a 5%, #68a54b 100%);
  background-color:#74ad5a;
  border-radius:6px;
  border:1px solid #3b6e22;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  
}
.myButton:hover {
  background:linear-gradient(to bottom, #68a54b 5%, #74ad5a 100%);
  background-color:#68a54b;
}
.myButton:active {
  position:relative;
  top:1px;
}
  #centro {
 text-align: center;
}
#texto {
  font-family: "Trebuchet MS", Verdana, sans-serif;
  margin-left:5%;
}

  </style>
  
</head>

<body>

  <?php


  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "agenda";


  $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
   die("Connection failed: " . mysqli_connect_error());
  }

                   
                  
  if (isset($_POST['volver'])){


  header("location:menu1.php");
  }

   if (isset($_POST['anadir'])){


  header("location:anadir.php");
  }

 if (isset($_POST['listar'])){


  header("location:listar.php");
  }

 if (isset($_POST['borrar'])){


  header("location:borrar.php");
  }

 if (isset($_POST['buscar'])){


  header("location:buscar.php");
  }

 if (isset($_POST['modificar'])){


  header("location:modificar.php");
  }

 if (isset($_POST['borrartodo'])){


  header("location:borrartodo.php");
  }
     session_start();
  if(isset($_SESSION["usuario"])){

     if (isset($_POST['cambiar'])) {
      header('location:modificar.php');
    }

  ?>


<div class="cabecera">


  <img src="imagenes/img1.png" width="90" height="90"><h1 id="texto1">&nbsp;Agenda de contactos </h1>
  <br><br><br>


  <form  action="" method="post">

   <input class="myButton3" type="submit" value="Volver" name="volver">
   <input class="myButton3" type="submit" value="Añadir registro"  name="anadir">
   <input class="myButton3" type="submit" value="Listar"  name="listar">
   <input class="myButton3" type="submit" value="Borrar"  name="borrar">
   <input class="myButton3" type="submit" value="Buscar"  name="buscar">
   <input class="myButton3" type="submit" value="Modificar"  name="modificar">
   <input class="myButton3" type="submit" value="Borrar todo"  name="borrartodo">
  </form>

</div>
    
<br><br><br><br>

    

    <?php
    
 

  if (!isset($_POST['modif'])) {

        $sql = "SELECT * from usuarios";

        $result = mysqli_query($conn, $sql);

        $valor = mysqli_num_rows($result);

        if ($result == TRUE) {
        ?>
            <form action="" method="post">

              <div id="centro">
                <img src="imagenes/img7.png" width="30" height="30"><h3 id="texto1"><b>&nbsp;&nbsp;Selecciona los contactos que quieras modificar de la agenda</b></h3><br>
              </div>

                <table>
                    <tr>
                        <th>Modificar</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                    </tr>
                    <?php
                    while ($registro = mysqli_fetch_row($result)) {
                    ?>
                        <tr align="center">
                            <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[2] ?>"></td>
                            <td><?php echo $registro[0] ?></td>
                            <td><?php echo $registro[1] ?></td>
                            <td><?php echo $registro[2] ?></td>
                            <td><?php echo $registro[3] ?></td>
                        </tr>

                    <?php
    
                    }
                    ?>
                </table>
                <br><br><div id="centro">
                <button class="myButton" type="submit" name="modif"> Modificar</button>
                <input class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">
            </form>
        <?php
        } else {
            echo "No se han encontrado Registros.";
        }
  }

        if (isset($_POST['modif'])) {
          if (isset($_POST['checkList'])) {
      

              $cuenta=count($_POST['checkList']);


            if ($cuenta==1){

              foreach ($_POST['checkList'] as $selected) {
                          $sql = "SELECT * FROM usuarios where telefono='$selected' ";
                          $result = mysqli_query ($conn, $sql);

                 if(mysqli_num_rows($result) > 0){
                   while ($registro = mysqli_fetch_row($result)) {

                    $_SESSION['nom']=$registro[0];

              ?>  
                    <div id="texto">
                        <img src="imagenes/img7.png" width="30" height="30"><p id="texto1"><b>&nbsp;&nbsp;Escribe los datos del nuevo registro</b></p><br>
                        <form action="" method="post">

                         <p><b>Nombre</b></p> <input class="campo" name="usuario" type="text" value="<?php echo $registro[0]?>">
                         <br><br>
                         <p><b>Apellidos</b></p> <input class="campo" name="apellido" type="text" value="<?php echo $registro[1] ?>">
                         <br><br>
                         <p><b>Teléfono</b></p> <input class="campo" name="tlf" type="text" value="<?php echo $registro[2] ?>">
                        <br><br>
                         <p><b>Correo</b></p><input class="campo" name="correo" type="text" value="<?php echo $registro[3] ?>">
                         <br><br>
                          

                         <input class="myButton" type="submit" value="Modificar"  name="cambiar">
                        <input class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">

                        </form>
                      </div>


              <?php           
                  
                  }
                } 
              }
            }else{
              echo "<h3 align='center'>Escoge solo un contanto a modificar</h3>";
            }
          }else{
            echo "<h3 align='center'>Debes escoger almenos un contacto para modificar</h3>";
          }
        }    

            if (isset($_POST['cambiar'])) {

               $n=$_SESSION['nom'];


                    $usuario=$_POST["usuario"];
                    $apellido=$_POST["apellido"];
                    $tlf=$_POST["tlf"];
                    $correo=$_POST["correo"];
              

                    $modif = " UPDATE usuarios SET nombre='$usuario', apellidos='$apellido', telefono='$tlf', correo='$correo' WHERE nombre='$n' ";
              $result1 = mysqli_query ($conn, $modif);

               if ($result1 == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";

              }
                              
              
                
                          
          } 
}  
            
      
    

mysqli_close($conn);
        ?>
</body>

</html>