<!DOCTYPE html>
<html>
<head>

<style>
#rojo{
	color:#EE280C;
}

#texto1{
  display: inline-block;
  vertical-align: top;
}

.cabecera{
  color:white;
  background-color:#6C969B;
  padding-left:15px;
  padding-bottom:15px;
}

.myButton3 {
  box-shadow:inset 0px 1px 0px 0px #ffffff;
  background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
  background-color:#ffffff;
  border-radius:6px;
  border:1px solid #dcdcdc;
  display:inline-block;
  cursor:pointer;
  color:#666666;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffffff;
}
.myButton3:hover {
  background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
  background-color:#f6f6f6;
}
.myButton3:active {
  position:relative;
  top:1px;
}

#texto {
  font-family: "Trebuchet MS", Verdana, sans-serif;
  margin-left:5%;
}
#centro {
 text-align: center;
}

table{
  
  border-collapse: collapse;
  background-color:#F5C274;
  border-radius: 15px;
  margin: auto;
}


</style>

</head>
<body>

<?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "agenda";


    $conn = mysqli_connect($servername, $username, $password,$dbname);

    if (!$conn) {
     die("Connection failed: " . mysqli_connect_error());
    }

  session_start();
  if(isset($_SESSION["usuario"])){


?>

<div class="cabecera">


  <img src="imagenes/img1.png" width="90" height="90"><h1 id="texto1">&nbsp;Agenda de contactos </h1>
  <br><br><br>


  <form  action="" method="post">

   <input class="myButton3" type="submit" value="Volver" name="volver">
   <input class="myButton3" type="submit" value="Añadir registro"  name="anadir">
   <input class="myButton3" type="submit" value="Listar"  name="listar">
   <input class="myButton3" type="submit" value="Borrar"  name="borrar">
   <input class="myButton3" type="submit" value="Buscar"  name="buscar">
   <input class="myButton3" type="submit" value="Modificar"  name="modificar">
   <input class="myButton3" type="submit" value="Borrar todo"  name="borrartodo">
  </form>

</div>
<br><br><br><br>
<div id="centro">
  <img src="imagenes/img3.png" width="30" height="30"><h3 id="texto1"><b>&nbsp;&nbsp;Lista de contactos existentes</b></h3><br>
</div>
<div id="texto">

  <?php



  		$sql = "SELECT nombre, apellidos, telefono, correo FROM usuarios ORDER BY nombre";

    $result = mysqli_query ($conn, $sql);


            if ($result == FALSE) {
            echo "Error en la ejecución de la consulta.<br />";

            }else {

                echo "<table style='width:70%'>";
                echo "<tr>";
                echo "<th>" ."Nombre "."</th>";
                echo "<th>" ."Apellidos "."</th>";
                echo "<th>" ." Telefono "."</th>";
                echo "<th>" ." Correo "."</th>";
                echo "</tr>";


              while ($registro = mysqli_fetch_row($result)) {
                echo "<tr align='center'>";
                echo "<td>" .$registro[0]."</td>";
                echo "<td>" .$registro[1]."</td>";
                echo "<td>" .$registro[2]."</td>";
                echo "<td>" .$registro[3]."</td>";
                echo "</tr>";


              }
              echo "</table>"."<br>";
            }

  }

  
  	

  	if (isset($_POST['volver'])){


    header("location:menu1.php");
    }

     if (isset($_POST['anadir'])){


    header("location:anadir.php");
    }

   if (isset($_POST['listar'])){


    header("location:listar.php");
    }

   if (isset($_POST['borrar'])){


    header("location:borrar.php");
    }

   if (isset($_POST['buscar'])){


    header("location:buscar.php");
    }

   if (isset($_POST['modificar'])){


    header("location:modificar.php");
    }

   if (isset($_POST['borrartodo'])){


    header("location:borrartodo.php");
    }


mysqli_close($conn);

  ?>

</div>
</body>
</html>