<!DOCTYPE html>
<html lang="en">

<head>
  <style>

    #texto1{
  display: inline-block;
  vertical-align: top;
}

.cabecera{
  color:white;
  background-color:#6C969B;
  padding-left:15px;
  padding-bottom:15px;
}

.myButton3 {
  box-shadow:inset 0px 1px 0px 0px #ffffff;
  background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
  background-color:#ffffff;
  border-radius:6px;
  border:1px solid #dcdcdc;
  display:inline-block;
  cursor:pointer;
  color:#666666;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffffff;
}
.myButton3:hover {
  background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
  background-color:#f6f6f6;
}
.myButton3:active {
  position:relative;
  top:1px;
}
.myButton3 {
  box-shadow:inset 0px 1px 0px 0px #ffffff;
  background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
  background-color:#ffffff;
  border-radius:6px;
  border:1px solid #dcdcdc;
  display:inline-block;
  cursor:pointer;
  color:#666666;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffffff;
}
table{
  
  border-collapse: collapse;
  background-color:#F5C274;
  border-radius: 15px;
  margin: auto;
  width:70%
}
#centro {
 text-align: center;
}
.myButton1 {
  box-shadow: 0px 1px 0px 0px #fff6af;
  background:linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
  background-color:#ffec64;
  border-radius:6px;
  border:1px solid #ffaa22;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffee66;
}
.myButton1:hover {
  background:linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
  background-color:#ffab23;
}
.myButton1:active {
  position:relative;
  top:1px;
}

.myButton2 {
  box-shadow: 0px 1px 0px 0px #fff6af;
  background:linear-gradient(to bottom, #EC5748 5%, #EA3F2D 100%);
  background-color:#EC5748;
  border-radius:6px;
  border:1px solid #EC5748;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #EC5748;
}
.myButton2:hover {
  background:linear-gradient(to bottom, #EA3F2D 5%, #EC5748 100%);
  background-color:#EA3F2D;
}
.myButton2:active {
  position:relative;
  top:1px;
}


  </style>
  
</head>

<body>

  <?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "agenda";


    $conn = mysqli_connect($servername, $username, $password,$dbname);

    if (!$conn) {
    die("Connection failed: ".mysqli_connect_error());
    }
          $sql = "SELECT * from usuarios";
          $result = mysqli_query($conn, $sql);
          $valor = mysqli_num_rows($result);

    if (isset($_POST['volver'])){

      header("location:menu1.php");
    }

    if (isset($_POST['anadir'])){

      header("location:anadir.php");
    }

    if (isset($_POST['listar'])){

      header("location:listar.php");
    }

    if (isset($_POST['borrar'])){

      header("location:borrar.php");
    }

    if (isset($_POST['buscar'])){

      header("location:buscar.php");
    }

    if (isset($_POST['modificar'])){

    header("location:modificar.php");
    }

    if (isset($_POST['borrartodo'])){

      header("location:borrartodo.php");
    }

    session_start();
    if(isset($_SESSION["usuario"])){
    if (isset($_POST['eliminar'])) {
      if (isset($_POST['checkList'])) {
        foreach ($_POST['checkList'] as $selected) {
          $sql = "DELETE from usuarios where telefono='$selected'";
          if(mysqli_query($conn, $sql)){
            header("location:borrar.php");
          }else{
          echo "error al borrar datos";
          }
        } 
      }
    }
  ?>

<div class="cabecera">

  <img src="imagenes/img1.png" width="90" height="90"><h1 id="texto1">&nbsp;Agenda de contactos </h1>
  <br><br><br>

  <form  action="" method="post">
   <input class="myButton3" type="submit" value="Volver" name="volver">
   <input class="myButton3" type="submit" value="Añadir registro"  name="anadir">
   <input class="myButton3" type="submit" value="Listar"  name="listar">
   <input class="myButton3" type="submit" value="Borrar"  name="borrar">
   <input class="myButton3" type="submit" value="Buscar"  name="buscar">
   <input class="myButton3" type="submit" value="Modificar"  name="modificar">
   <input class="myButton3" type="submit" value="Borrar todo"  name="borrartodo">
  </form>

</div>
    
<br><br><br><br>
<div id="centro">
  <img src="imagenes/img4.png" width="30" height="30"><h3 id="texto1"><b>&nbsp;&nbsp;Selecciona los contactos que quieras borrar de la agenda</b></h3><br>
</div>


<?php

  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "agenda";


  $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
   die("Connection failed: ".mysqli_connect_error());
  }
        $sql = "SELECT * from usuarios";
        $result = mysqli_query($conn, $sql);
        $valor = mysqli_num_rows($result);

        if ($result == TRUE) {
?>
          <form action="" method="post">
            <table>
              <tr>
                <th>Borrar</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Telefono</th>
                <th>Correo</th>
                </tr>

<?php
                while ($registro = mysqli_fetch_row($result)){
?>
                <tr align="center">
                    <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[2] ?>"></td>
                    <td><?php echo $registro[0] ?></td>
                    <td><?php echo $registro[1] ?></td>
                    <td><?php echo $registro[2] ?></td>
                    <td><?php echo $registro[3] ?></td>
                </tr>
<?php
                    }
?>
                </table>
                <br><br><div id="centro">
                  <button class="myButton2" type="submit" name="eliminar"> Borrar</button>
                  <input  class="myButton1" type="reset" name="Submit" value="Reiniciar formulario">
                </div>
            </form>
<?php
        } else {
            echo "No se ha nencontrado Registros.";
        }
  }

  mysqli_close($conn);
?>
</body>

</html>
