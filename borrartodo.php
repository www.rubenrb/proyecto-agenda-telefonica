<!DOCTYPE html>
<html>
<head>

<style>
#texto1{
  display: inline-block;
  vertical-align: top;
}

.cabecera{
  color:white;
  background-color:#6C969B;
  padding-left:15px;
  padding-bottom:15px;
}

.myButton3 {
  box-shadow:inset 0px 1px 0px 0px #ffffff;
  background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
  background-color:#ffffff;
  border-radius:6px;
  border:1px solid #dcdcdc;
  display:inline-block;
  cursor:pointer;
  color:#666666;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #ffffff;
}
.myButton3:hover {
  background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
  background-color:#f6f6f6;
}
.myButton3:active {
  position:relative;
  top:1px;
}

.myButton {
  box-shadow: 0px 1px 0px 0px #9acc85;
  background:linear-gradient(to bottom, #74ad5a 5%, #68a54b 100%);
  background-color:#74ad5a;
  border-radius:6px;
  border:1px solid #3b6e22;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  
}
.myButton:hover {
  background:linear-gradient(to bottom, #68a54b 5%, #74ad5a 100%);
  background-color:#68a54b;
}
.myButton:active {
  position:relative;
  top:1px;
}
.myButton2 {
  box-shadow: 0px 1px 0px 0px #fff6af;
  background:linear-gradient(to bottom, #EC5748 5%, #EA3F2D 100%);
  background-color:#EC5748;
  border-radius:6px;
  border:1px solid #EC5748;
  display:inline-block;
  cursor:pointer;
  color:#333333;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #EC5748;
}
.myButton2:hover {
  background:linear-gradient(to bottom, #EA3F2D 5%, #EC5748 100%);
  background-color:#EA3F2D;
}
.myButton2:active {
  position:relative;
  top:1px;
}
#centro {
 text-align: center;
}

</style>

</head>
<body>
<?php
  session_start();
  if(isset($_SESSION["usuario"])){


?>

<form  action="" method="post">
  <div class="cabecera">
     <img src="imagenes/img1.png" width="90" height="90"><h1 id="texto1">&nbsp;Agenda de contactos </h1>
       <br><br><br>

    <input class="myButton3" type="submit" value="Volver" name="volver">
    <input class="myButton3" type="submit" value="Añadir registro"  name="anadir">
    <input class="myButton3" type="submit" value="Listar"  name="listar">
    <input class="myButton3" type="submit" value="Borrar"  name="borrar">
    <input class="myButton3" type="submit" value="Buscar"  name="buscar">
    <input class="myButton3" type="submit" value="Modificar"  name="modificar">
    <input class="myButton3" type="submit" value="Borrar todo"  name="borrartodo">


    </div><br><br>
    <div id="centro">
      <img src="imagenes/img6.png" width="30" height="30"><h3 id="texto1"><b>&nbsp;&nbsp;¿Estas seguro de querer borrar toda la agenda de contactos?</b></h3><br>
      <br>
      <input class="myButton" type="submit" value="Si"  name="si">
      <input class="myButton2" type="submit" value="No"  name="no">
  </div>
  <br><br><br><br>
</form>
<?php
}


    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "agenda";


    $conn = mysqli_connect($servername, $username, $password,$dbname);

    if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    }

  if (isset($_POST['si'])){

      $sql = "DELETE FROM usuarios";

    
      if (mysqli_query($conn, $sql)) {
          echo "<br><h3 align='center'>Se ha borrado toda la agenda de contactos</h3>";
        }else{
          echo "<br><p> Error al borrar el contenido </p>";
        }


      }

  if (isset($_POST['no'])){

      header("location:menu2.php");
  }
    
  if (isset($_POST['volver'])){

      header("location:menu1.php");
  }

  if (isset($_POST['anadir'])){

      header("location:anadir.php");
  }

  if (isset($_POST['listar'])){


    header("location:listar.php");
  }

  if (isset($_POST['borrar'])){


      header("location:borrar.php");
  }

  if (isset($_POST['buscar'])){

    header("location:buscar.php");
  }

  if (isset($_POST['modificar'])){

    header("location:modificar.php");
  }

  if (isset($_POST['borrartodo'])){

    header("location:borrartodo.php");
  }
    mysqli_close($conn);
  ?>

</div>
</body>
</html>
